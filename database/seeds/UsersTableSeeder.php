<?php

use Illuminate\Database\Seeder;
use App\Admin;
use App\Roles;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Admin::truncate();
        DB::table('admin_roles')->truncate();

        $adminRoles = Roles::where('name','admin')->first();

        $admin = Admin::create([
        	'admin_name' => 'admin',
        	'email' => 'huy@gmail.com',
        	'admin_phone' => '123456789',
        	'password' => Hash::make('huy'),
        ]);

        $admin->roles()->attach($adminRoles);

    }
}
