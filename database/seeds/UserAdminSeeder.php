<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserAdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            [
                'id' => '100',
                'name' => 'Chu Văn Huy',
                'email' => 'vanhuyutehy@gmail.com',
                'password' => Hash::make('vanhuyutehy'),
            ],

        ]);
    }
}
